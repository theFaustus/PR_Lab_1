import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created by Faust on 2/21/2017.
 */
public class ComplexNumberSerializer {
    public static final File XML_DIRECTORY = new File("XML/");
    public static final File SERIALIZED_COMPLEX_NUMBERS = new File(XML_DIRECTORY, "complex_numbers.xml");
    private Persister persister = new Persister();

    public void serialize(ComplexNumber complexNumber){
        try{
            if(!XML_DIRECTORY.exists())
                XML_DIRECTORY.mkdirs();
            if(!SERIALIZED_COMPLEX_NUMBERS.exists()){
                SERIALIZED_COMPLEX_NUMBERS.createNewFile();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(SERIALIZED_COMPLEX_NUMBERS);
            persister.write(complexNumber, fileOutputStream);
            fileOutputStream.close();
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    public ComplexNumber deserialize(){
        try{
            if(!SERIALIZED_COMPLEX_NUMBERS.exists()){
                serialize(new ComplexNumber());
            }
            FileInputStream fileInputStream = new FileInputStream(SERIALIZED_COMPLEX_NUMBERS);
            ComplexNumber complexNumber = persister.read(ComplexNumber.class, fileInputStream);
            fileInputStream.close();

            return complexNumber;
        } catch (Exception e){
            throw new RuntimeException(e);
        }

    }


}
