/**
 * Created by Faust on 2/21/2017.
 */
public class SimpleXMLDemo {
    public static void main(String[] args) {
        ComplexNumber a = new ComplexNumber(2, 3);
        System.out.println("a = " + a);

        ComplexNumberSerializer complexNumberSerializer = new ComplexNumberSerializer();
        complexNumberSerializer.serialize(a);

        ComplexNumber b = complexNumberSerializer.deserialize();
        System.out.println("b = " + b);

    }
}
