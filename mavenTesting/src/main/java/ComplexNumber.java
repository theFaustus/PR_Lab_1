import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * Created by Faust on 2/21/2017.
 */
@Root
public class ComplexNumber implements Serializable{
    @Element
    private double real;
    @Element
    private double img;

    public ComplexNumber(double real, double img) {
        this.real = real;
        this.img = img;
    }

    public double getReal() {
        return real;
    }

    public void setReal(double real) {
        this.real = real;
    }

    public double getImg() {
        return img;
    }

    public void setImg(double img) {
        this.img = img;
    }

    ComplexNumber(){
        this.real = 0;
        this.img = 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ComplexNumber that = (ComplexNumber) o;

        if (Double.compare(that.getReal(), getReal()) != 0) return false;
        return Double.compare(that.getImg(), getImg()) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getReal());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getImg());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public String toString(){
        return String.format("( %.2f, %.2fi)", real, img);
    }
}
